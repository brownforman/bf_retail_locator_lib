# B-F Retail Locator PHP Library

## Using with Composer
### composer.json Example

```
"require": {
	"bf/rlservices": "dev-master"
},
"repositories": [{
    "type": "vcs",
    "url": "https://bitbucket.org/brownforman/bf_retail_locator_lib.git"
}]
```
### Constructor Example
```
//Testing Dataset
$rlws = new BF\RLServices\BFRLServices("proof");

//Production Dataset
$rlws = new BF\RLServices\BFRLServices("prod");
```

## Fetch a List of Minor Brands
```
//Get a brand ID from B-F IT
$rlws->getMinorBrands($brand_id)
```
## Get Retail Locations
```
$params = array(
	'sDistance' => 10, //Distance in miles
	'sPrimaryCity' => "Louisville",
	'sState' => "KY",
	'sPostalCode' => "40202",
	'sSiteID' => 1 //Get this number from B-F IT pass as an INT
);
```
## List of SiteID's
SiteID	SiteDescription

1	www.b-fonline.com

2	www.littleblackdresswines.com

3	www.southerncomfort.com

4	www.southerncomfort.com/facebook

5	www.eljimador.com

6	www.herradura.com

7	www.eljimador.com/iphone

8	www.chambordonline.com

9	www.chambordonline.com/iphone

10	www.tuaca.com

12	www.woodfordreserve.com

13	m.eljimador.com

14	m.herradura.com

15	mobile www.woodfordreserve.com

16	www.lbdvodka.com

17	www.korbel.com

18	mobile.b-f.com/sonoma

19	mobile Tuaca 

20	www.oldforester.com

21	www.earlytimes.com

22	www.sonomacutrer.com

23	www.cheerstothehost.com

24	www.earlytimes.com

25	www.Finlandia.com

27	www.OldForester.com

28	www.sonomacutrer.com

29	www.chambordchannel.com

30	www.collingwoodwhisky.com