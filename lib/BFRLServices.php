<?php

/***********************************************************
// BF Retail Locator WebServices Connection Class
// Created By	: Arthur Masterson
// Date			: October 7, 2013
***********************************************************/
namespace BF\RLServices;

class BFRLServices {

	var $endpoint = "http://webservice.brown-forman.com/BFRetailLocator/BFRetailLocator.asmx?WSDL";
	var $live_endpoint = "http://webservice.brown-forman.com/BFRetailLocator/BFRetailLocator.asmx?WSDL";
	var $envelope = "http://schemas.xmlsoap.org/soap/envelope/";

	var $client;
	var $client_options = array();

	public function __construct($mode="proof",$trace=0,$cache_wsdl=0,$exceptions=0)
    {
		if($mode != "proof") $this->endpoint = $this->live_endpoint;

		$this->client_options["trace"] = $trace;
		$this->client_options["cache_wsdl"] = $cache_wsdl;
		$this->client_options["exceptions"] = $exceptions;
		$this->client = new \SoapClient($this->endpoint, $this->client_options);
    }

	private function kvToOptions($kv, $value) {
		$options = "";
		foreach($kv as $val=>$text) {
			$selected = ( trim($val) == $value) ? "selected" : "";
			$options.=sprintf('<option value="%s" %s>%s</option>', trim($val), $selected, trim($text));
		}
		return $options;
	}

	/****************************************************************************************************
	//AllBrands
	****************************************************************************************************/
	public function getBrandsOptionsLimited($parms) {
		//error_log(__LINE__);
		$brand_id = "";
		$brand_list = array();

		if (array_key_exists("brand_id", $parms)) {
			$brand_id = $parms["brand_id"];
		}

		if (array_key_exists("brand_list", $parms) && is_array($parms["brand_list"])) {
			$brand_list = $parms["brand_list"];
		}

		$brands = $this->getBrands();
		$brands_final = array();
		foreach($brands as $k=>$v) {
			$k = trim($k);
			$v = trim($v);
			if (in_array($k, $brand_list)) {
				$brands_final[$k] = $v;
			}
		}

		asort($brands_final);
		return $this->kvToOptions($brands_final, $brand_id);

	}

	public function getBrandsOptions($brand_id = "") {
		$brands = $this->getBrands();
		asort($brands);
		return $this->kvToOptions($brands, $brand_id);
	}

	public function getBrands()
	{
		$brands = array();

		try{
			$result = $this->client->GetBrands();
			$xml = $result->GetBrandsResult->any;

			$response = simplexml_load_string($xml, NULL, false, $this->envelope);

			$brand_id = $response->xpath('//BrownFormanBrands/Table/BrandID');
			$brand_name = $response->xpath('//BrownFormanBrands/Table/BrandName');

			for($i=count($brand_name)-1; $i>=0; $i--){
				$key = (string) $brand_id[$i];
				$value = (string) $brand_name[$i];
				$brands[$key] = $value;
			}
		}
		catch(Exception $e){
			$brands = array();
		}

		return $brands;
	}

	/****************************************************************************************************
	//MinorBrands
	****************************************************************************************************/
	public function getMinorBrandsOptions($brand = "1", $minorbrand_id = "") {
		$minorbrands = $this->getMinorBrands($brand);
		asort($minorbrands);
		return $this->kvToOptions($minorbrands, $minorbrand_id);
	}

	public function getMinorBrands($brand)
	{
		$minorbrands = array();

		try{
			$params = array("sBrandID"=>$brand);
			$result = $this->client->MinorBrands($params);
			$xml = $result->MinorBrandsResult->any;

			$response = simplexml_load_string($xml, NULL, false, $this->envelope);

			$minorbrand_id = $response->xpath('//BrownFormanBrandsMinor/Table/BrandMinorID');
			$minorbrand_name = $response->xpath('//BrownFormanBrandsMinor/Table/BrandMinorDescription');

			for($i=count($minorbrand_name)-1; $i>=0; $i--){
				$key = (string) $minorbrand_id[$i];
				$value = (string) $minorbrand_name[$i];
				$minorbrands[$key] = $value;
			}
		}
		catch(Exception $e){
			$minorbrands = array();
		}

		return $minorbrands;
	}

	/****************************************************************************************************
	//RetailLocations
	****************************************************************************************************/
	public function getRetailLocations($params = array())
	{
		$retaillocations = array();

		$result = $this->client->RetailLocations($params);
		$xml = $result->RetailLocationsResult->any;

		$response = simplexml_load_string($xml, NULL, false, $this->envelope);
		$rl_distance = $response->xpath('//BFRetailSearch/Property/Distance');
		$rl_company = $response->xpath('//BFRetailSearch/Property/Company');
		$rl_phone = $response->xpath('//BFRetailSearch/Property/Phone');
		$rl_latitude = $response->xpath('//BFRetailSearch/Property/Latitude');
		$rl_longitude = $response->xpath('//BFRetailSearch/Property/Longitude');
		$rl_address = $response->xpath('//BFRetailSearch/Property/Address');
		$rl_city = $response->xpath('//BFRetailSearch/Property/City');
		$rl_state = $response->xpath('//BFRetailSearch/Property/State');
		$rl_postalcode = $response->xpath('//BFRetailSearch/Property/PostalCode');
		$rl_countryregion = $response->xpath('//BFRetailSearch/Property/CountryRegion');

		for($i=count($rl_distance)-1; $i>=0; $i--){
			$data['distance'] = (string) $rl_distance[$i];
			$data['company'] = (string) $rl_company[$i];
			$data['phone'] = (string) $rl_phone[$i];
			$data['latitude'] = (string) $rl_latitude[$i];
			$data['longitude'] = (string) $rl_longitude[$i];
			$data['address'] = (string) $rl_address[$i];
			$data['city'] = (string) $rl_city[$i];
			$data['state'] = (string) $rl_state[$i];
			$data['postalcode'] = (string) $rl_postalcode[$i];
			$data['countryregion'] = (string) $rl_countryregion[$i];
			$retaillocations[] = $data;
		}

		return $retaillocations;
	}


}